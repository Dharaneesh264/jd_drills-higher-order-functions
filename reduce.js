
const reduce = (input, callback, start) => {
    if (input === undefined || input.length === 0) {
        return undefined;
    }
    let ans = 0;
    if (typeof callback === 'function') {
        // let i = 0;
        for (let i = 0; i < input.length; i++) {
            if (start === undefined) {
                start = input[i];
                continue;
            }
            start = callback(start, input[i], i, input);
        }
        return start;
    }
}


module.exports = reduce;
