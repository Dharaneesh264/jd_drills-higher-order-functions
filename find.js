const find = (input, ele, callback) => {
    if (input === undefined || input.length === 0)
        return undefined;
    if (callback != undefined) {
        for (let i = 0; i < input.length; i++) {
            if (callback(input, i, ele) === true) {
                // console.log('hello', input[i]);
                console.log(true);
                return;
            }
            // return;
        }
        console.log(false);
    } else {
        for (let i = 0; i < input.length; i++) {
            if (input[i] === ele) {
                return true;
            }
        }
        return false;
    }

}
module.exports = find;