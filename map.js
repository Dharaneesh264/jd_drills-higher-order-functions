const map = (input, callback) => {
    if (input === undefined || input.length === 0) {
        return [];
    }
    ans = [];
    if (typeof callback === 'function') {
        for (let i = 0; i < input.length; i++) {
            ans.push(callback(input[i], i, input));
        }
        return ans;
    }
    return ans;
}
module.exports = map;