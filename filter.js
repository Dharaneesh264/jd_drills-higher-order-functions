const filter = (input, cb) => {
    if (input === undefined || input.length === 0) {
        return [];
    }
    const ans = [];
    if (cb != undefined) {
        for (let i = 0; i < input.length; i++) {
            let res = cb(input, i);
            if (res != undefined) {
                ans.push(res);
            }
        }
        return ans;
    } else {
        for (let i = 0; i < input.length; i++) {
            let ele = input[i];
            if (ele > 3)
                ans.push(ele);
        }
        return ans;
    }
}

module.exports = filter;