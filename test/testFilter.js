const filter = require('../filter');



const input = [1, 2, 3, 4, 5, 6, 7];
const cb = (input, i) => {
    if (input[i] > 3) {
        return input[i];
    }
}

console.log(filter(input, cb));

console.log(filter(input));
console.log(filter([]));
console.log(filter());