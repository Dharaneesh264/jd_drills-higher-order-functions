const each = require('../each');

const inputArray = [1, 2, 3, 4, 5, 5];

const cb = (input, i) => {
    console.log(input[i]);
}

each(inputArray, cb);

each(inputArray);

console.log(each([]));
console.log(each());
// 
