const find = require('../find');

const input = [1, 2, 3, 4, 5, 5];


const cb = (input, i, ele) =>  {
    return input[i] === ele;
}

find(input, 4, cb); // true

console.log(find(input, 4)); // true
console.log(find([], 10)); // false