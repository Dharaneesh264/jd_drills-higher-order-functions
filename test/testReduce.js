const reduce = require('../reduce');

const callback = (start, inputValue, index, input) => {
     return start + inputValue * inputValue * inputValue;
}

const input = [1, 2, 3, 4, 5];
console.log(reduce(input, callback, 10));
console.log(reduce([], callback, 10));
console.log(reduce(input, callback));
console.log(reduce([], callback))