const map = require('../map')

const input = [1, 2, 3, 4, 5, 5];
const result = (inputValue, index, input) => {
    return inputValue * inputValue * inputValue;
}

console.log(map(input, result));
console.log(map(input));
console.log(map());