const each = (input, callback) => {
  if (input === undefined || input.length === 0) {
    return 'empty input or no input passed';
  }

  if (callback != undefined) {
    for (let i = 0; i < input.length; i++) {
      callback(input, i);
    }
  } else {
    for (let i = 0; i < input.length; i++) {
      console.log(input[i]);
    }
  }
}

module.exports = each;